# CI

This project has automated artifact generation.
Push a tag named `release/<version>` to trigger artifact generation.

Use tag ``test/<version>`` when testing new ``.gitlab-ci.yml`` configurations

``version value has to be increased, otherwise nothing will happen!``

> The new version scheme follows the pattern `<yy>.<minor>.<patch>`

# Website
This project is built with the [Hugo Framework](https://gohugo.io/).

The used theme is [Hugo Massively](https://themes.gohugo.io/hugo-theme-massively/)

## Development

### Local Development
```bash
npm install
npm run serve
```

### Publish new version
```bash
# commit everything
git commit <...>

# push changes
git push

# set tag for ci: e.g. 'git tag -a release/1.1.0 'add content for 2021' '
git tag -a <tagname> -m '<message>'

# push tag to remote to trigger CI: e.g. 'git push origin release/1.1.0
git push origin <tagname>
```

publishing can take up to 15 minutes

## Description of special content

### Schedule
Schedule is generated from ``*.json`` files. 

For an example look for ``data/schedule.json``.

When ``schedule = '<link to json>'`` is present in post-header, a table will be generated as first Element below the heading.
All content of that ``*.md``-file is placed below.

Note: Add empty entries for time slots without content. Do not overlap times. 

### Balance / Cost
the script located in ``static/scripts/cost.js`` reads contents from a Google Spreadsheet on page load. 

Link to Spreadsheet: ``https://docs.google.com/spreadsheets/d/1cj9YkbK3IqBwp9lWjxzcXiw9KvbzAXY7lb02XhE2YcY/edit?usp=sharing``
Link is also available at [`<domain>/admin.html`](https://www.legendary-ophase.de/admin.html) (https://www.legendary-ophase.de/admin.html)

- div with id ``cost-div`` is required.
- div with id ``cost-table`` is optional. If available a detailed list of entries is displayed here.

Note: Detailed entries are not *really* hidden when ``cost-table`` doesn't exist, they are simply not shown on the page.

### Custom classes
located in ``static/assets/css/custom.css``
- ``bigger`` emphasizes content in element
- ``timetable`` pretty formats table for timetables

add your custom classes to this file at the bottom if needed.