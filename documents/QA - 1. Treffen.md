# Fragen erstes Treffen


F: Gibt es im Anwendungsfach Übungsblätter?
A: Kommt darauf an, steht im Modulhandbuch

F: Klausuranmeldung?
A: Wird je Vorlesung bekanntgegeben in der Vorlesung

F: Orientierungsprüfung? - Mathe
A: La1 + Ana1

F: Orientierungsprüfung? - Lehramt
A: entweder wie Mathe ODER vom zweiten Fach

F: Lerngruppen
A: Selbst suchen! Nichts offizielles, aber WICHTIG!

F: Wer muss IAM (Programmieren) machen?
A: Mathe, Wirtschafdtsmathe, Technomathe. NICHT: Lehramt

F: Tutorien (kurz: "Tut") Anmeldung?
A: Wird in der ersten Vorlesung gesagt. Generell gilt: Tutorium > Vorlesung

F: Tutorien vor Ort?
A: Evtl ja, aber immer auch online. Beginn: Zweite Woche

F: Anwendungsfach Wirtschaft:
A: VWL1 + 2 oder BWL1-4 abschließen, mischen ist aber möglich 

F: Anwendungsfach Info:
A: Muss: GBI + Algo

F: Wirtschaftmathe und andere:
A: Siehe Modulhandbuch.
- Mathe: https://www.math.kit.edu/lehre/seite/modulhandb/media/studienplan-bachelor-tema.pdf
- Wirtschaftsmathe: https://www.math.kit.edu/lehre/seite/modulhandb/media/mhb-bachelor-wima.pdf
- Alle anderen: http://letmegooglethat.com/?q=KIT+modulhandbuch+%3Cdein+studiengang%3E

F: Noten im Studium:
A: https://www.4-gewinnt.de/

F: Ab wann Übungen?
A: Unterschiedlich. Meistens erst nach der ersten Vorlesung.

F: Skript?
A: Gibt es nicht immer! Wird zur Verfügung gestellt (fast immer als PDF)
