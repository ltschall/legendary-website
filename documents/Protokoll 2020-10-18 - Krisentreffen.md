# Protokoll 2020-10-18 - Krisentreffen

Um 14:00 Uhr unter https://meet.vs.kit.edu/b/tob-hww-civ.

## Anmerkungen

Das hier sollte möglichst weniger eine Präsentation, als ein Gespräch werden. Die CoronaVO und das KIT lassen keine Kleingruppen zu (bei 612 Erstis dieses Semester).

## Kooperationen & Konzepte

* Lila Pause & HighTech bauen den Campus in Minecraft nach
* Eindringlich den Erstis vermitteln „TREFFT EUCH VERDAMMT NOCH MAL NICHT!“ (neue Generation geht normalerweise super mit Autorität um)
* FSMI & Dennis (Lila Pause) sind zur Hilfe beim Hosting bereit
* Campus-Tour wird gerade aufgezeichnet
* Tägliche Streams seitens First Contact Gecko
* Malibu ist hauptsächlich auf Offline ausgelegt, und funktioniert nicht Online (hoffnungslos) (darauf erstmal nen Frust-Korn) (Legendary auch 🥃) RIP Malibu F F F F eff f :'(
* Alle besitzen einen aktiv genutzten Discord (außer Malibu)
* Jozi will 24/7-Livestream von seinen Erstis. Jozi redet sehr gerne, sehr viel, wir haben ihn aber trotzdem sehr lieb. ❤️❤️❤️
* Streams von Montag müssen auf Dienstag verschoben werden
* Streamingplan an die Orga versenden
* First Come, First Serve für die Zuteilung am Montag zu einem festgelegten Zeitpunkt

## Größe der Gruppen

* Arrrrr!: 80-100
* First Contact: 250
* HiGHtech: 100–120
* Legendary: 60
* Lila Pause: 100-110
* Malibu: 0
* Party-PiPle: 80-100
* Pirates: 90
* Project Pi: 80–100
* Second Contact: ∞
* Studier Langsam: 100–110

# Links

Halbwegs wichtige Verlinkungen:

* CoronaVO: https://www.baden-wuerttemberg.de/fileadmin/redaktion/dateien/PDF/Coronainfos/201018_CoronaVO_konsolidierte_Fassung_ab_201019.pdf
* KIT-Corona-FAQ (hoffentlich bald mit Informationen): https://www.kit.edu/kit/25911.php

Spiele:

* Skibbl.io https://skribbl.io/
* Sketchful.io https://sketchful.io/ (läuft zeitweise stabiler als skribbl)
* Werwolf http://www.werwolfonline.eu/info/index.php/anleitung/ablauf-eines-spiels
* Secret Hitler https://private.secrethitler.io/
* Cameleon https://tabletopia.com/games/chameleon-board-game
* Codenames https://www.horsepaste.com/
* Cards against Humanity https://cardsagainstformality.io/rooms
* Tabletopia https://tabletopia.com/

