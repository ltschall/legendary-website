FROM nginx:alpine

COPY public/ /usr/share/nginx/html/
COPY docker/default.conf /etc/nginx/conf.d/default.conf

COPY docker/entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
