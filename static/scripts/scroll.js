window.addEventListener("load", function () {
    document.querySelectorAll(".timetable").forEach(e => {
        scrollTable(e)
    })
})

async function scrollTable(e) {
    e.scroll({
        left: 45,
        behavior: "smooth"
    })
    await wait(333);
    e.scroll({
        left: 0,
        behavior: "smooth"
    })
    await wait(333);
    e.scroll({
        left: 25,
        behavior: "smooth"
    })
    await wait(333);
    e.scroll({
        left: 0,
        behavior: "smooth"
    })
}

async function wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}