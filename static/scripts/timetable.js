function refreshTable() {
  if (mobile.matches) {
    function mobileView(cell) {
      let day = cell.getAttribute('day');
      if (day && dayCells.indexOf(day) === visibleDay) {
        cell.style.display = '';
      } else {
        cell.style.display = 'none';
      }
    }

    document.querySelectorAll(`td[day]`).forEach(cell => mobileView(cell));
    document.querySelectorAll(`th[day]`).forEach(cell => mobileView(cell));
  } else {
    document.querySelectorAll(`td[day]`).forEach(cell => cell.style.display = '');
    document.querySelectorAll(`th[day]`).forEach(cell => cell.style.display = '');
  }
  const paddedHours = String(hours).padStart(2, '0')
  if (minutes === 0) {
    document.querySelector(`[time="${paddedHours}:00"]`).style.backgroundColor = 'red';
  } else {
    document.querySelector(`[time="${paddedHours}:30"]`).style.backgroundColor = 'red';
  }
}

function refreshNavigation() {
  let div = document.querySelector('#timetable-navigation');
  if (mobile.matches) {
    console.log('mobile');
    div.style.display = '';
  } else {
    console.log('desktop');
    div.style.display = 'none';
  }
}

function refresh() {
  refreshNavigation();
  refreshTable();
}

function minusDay() {
  visibleDay = ((visibleDay - 1) + 7) % 7;
  refreshTable();
}

function plusDay() {
  visibleDay = (visibleDay + 1) % 7;
  refreshTable();
}

let dayCells = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'];
let hourCells = [];
for (let h = 8; h <= 23; h++) {
  for (let m = 0; m <= 30; m += 30) {
    if (m === 0) {
      hourCells.push(`${h}:00`);
    } else {
      hourCells.push(`${h}:30`);
    }
  }
}

let weekday = ((new Date()).getDay() - 1 + 7) % 7;
let visibleDay = weekday;
if (visibleDay >= 5) visibleDay = 0;
let hours = (new Date()).getHours();
let minutes = Math.floor((new Date()).getMinutes() / 30);
let mobile = window.matchMedia("(max-width: 1000px)");

window.addEventListener('resize', refresh);
window.addEventListener('load', refresh);