function trim(str) {
    return str.trim().replaceAll('€', '')
}

function createRow(entry) {
    return `<tr><td>${trim(entry[0])}</td><td>${trim(entry[1])}</td><td style="white-space:nowrap">${trim(entry[2])} EUR</td></tr>`
}

function show(data) {
    console.log(data)
    if (!data.errors.length) {
        const divDom = document.getElementById('cost-div')
        const balance = trim(data.data[0][5])
        divDom.innerHTML = balance + ' EUR'
        if (balance.startsWith('-')) {
            divDom.style.color= "red"
        }



        const tableDom = document.getElementById('cost-table')
        if (tableDom) {
            let tableData = [...data.data]
            tableData.shift()
            console.log(tableData);
            tableData = tableData.filter(row => row[0] || row[1] || row[2])
            console.log(tableData);
            tableData = tableData.reverse()
            const rows = tableData.map(createRow).join('')
            tableDom.innerHTML = `<table>${rows}</table>`
        }
    }
}

function load() {
    const csvUrl = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQRlvMn9i_gjOfUBY05_Sq4oKJqte0xsBtbuxaIrBbo13BdOYf2MSY-OVJZoDcSlaRIks54PM-rH_by/pub?output=csv'
    Papa.parse(csvUrl, { download: true, complete: show })
}
window.onload = load
