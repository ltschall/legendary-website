+++
weight = '005'
title = 'das Wichtigste auf einen Blick'
slug = 'featured'
image = 'images/gruppe-schloss.jpg'
description = ''
disableComments = true
+++

<iframe class="video" width="640" height="360" src="https://www.youtube-nocookie.com/embed/fsyez_P3fnQ?start=12&end=18" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="margin-bottom: 1rem"></iframe>


## Legendary
Ihr wollt, dass eure O-Phase legendär wird? Dann seid ihr bei uns genau richtig! Bei unserer selbstorganisierten Olympiade könnt ihr nicht nur eure (Trink-)Fähigkeiten unter Beweis stellen, es ist auch wie alle anderen Aktivitäten die beste Gelegenheit, eure Kommilitonen besser kennen zu lernen. Neben Kennenlernspielen, einer Campusführung und nützlichen Infos zum Studium warten unter anderem auch Cocktails würfeln, ein Flammkuchen All-You-Can-Eat sowie Lasertag auf euch. Und abends geht's dann in die Clubs, die Karlsruhe zu bieten hat. Also worauf wartet ihr? It’s going to be Legen… Wait for it… dary!

Einen Überblick über alle Aktivitäten findet ihr [hier.](/post/wochenplan/)
Um immer auf dem aktuellen Stand zu sein, tretet [hier](/post/whatsapp/) unserer WhatsApp-Gruppe bei.
