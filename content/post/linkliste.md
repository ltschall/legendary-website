+++
weight = '020'
title = 'Hilfreiche Links'
slug = 'linkliste'
image = 'images/linkliste.jpg'
description = 'Hier findest du einige hilfreiche Links fürs Studium.'
disableComments = true
+++
- **Vorlesungsverzeichnis**  
-- https://campus.studium.kit.edu/events/catalog.php#!campus/all/fields.asp?group=Vorlesungsverzeichnis
- **Prüfungsanmeldung, Studienbescheinigung, Rückmeldung, etc.**  
-- https://campus.studium.kit.edu/ 
- **Foren für viele Vorlesungen (Skripte, Übungsblätter, etc.)**  
-- https://ilias.studium.kit.edu
- **Modulhandbücher**  
-- Info: https://www.informatik.kit.edu/formulare.php  
-- Mathe: http://www.math.kit.edu/lehre/seite/modulhandb/de
- **Klausurtermine Informatik**  
-- https://www.informatik.kit.edu/9581.phps
- **Fachschaft Mathe-Info**  
-- https://www.fsmi.uni-karlsruhe.de/
- **Studierendenservice**  
--https://www.sle.kit.edu/wirueberuns/studierendenservice_oeffnungszeiten.php 
- **Campusplan**  
-- https://www.kit.edu/campusplan/  
-- https://www.kithub.de/map/2210  
- **PC-Nutzung, Drucker**  
-- https://atis.informatik.kit.edu/  
-- https://www.scc.kit.edu/