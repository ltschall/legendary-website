+++
weight = '035'
title = 'Whatsapp - Gruppe'
slug = 'whatsapp'
image = 'images/whatsapp.jpg'
description = 'Tritt unserer Whatsapp-Gruppe bei!'
disableComments = true
+++

Unserer Whatsapp-Gruppe kannst du unter folgendem Link beitreten:\
https://chat.whatsapp.com/BlG2VrAQPzQ6NFH0SM5uBy
