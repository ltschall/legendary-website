+++
weight = '015'
title = 'Kostenübersicht'
slug = 'kosten'
image = 'images/kosten.jpg'
description = 'Hier siehst du unseren aktuellen Kassenstand.'
disableComments = true
+++

### Kontostand
<div id="cost-div" style="margin: 0 auto; padding: 0 1em; text-align: center; font-size: 2em"></div>

### Kontoauszug
*[neue Einträge zuerst]*
<div id="cost-table" style="margin: 0 auto; padding: 0 1em; text-align: left"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/PapaParse/5.3.1/papaparse.min.js" integrity="sha512-EbdJQSugx0nVWrtyK3JdQQ/03mS3Q1UiAhRtErbwl1YL/+e2hZdlIcSURxxh7WXHTzn83sjlh2rysACoJGfb6g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="/scripts/cost.js"></script>
