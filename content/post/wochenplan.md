+++
weight = '010'
title = 'Wochenplan'
slug = 'wochenplan'
image = 'images/wochenplan.jpg'
description = 'Unser Plan der O-Phase auf einen Blick.'
schedule = 'data/schedule.json'
disableComments = true
+++

## Montag
{{%expand "Infos" %}}
### Infobau (Seminarraum)
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2616.8297596216667!2d8.417570516089366!3d49.01382367930303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47970633960ce621%3A0xa47be102bfa7364!2sInformatik-Hauptgeb%C3%A4ude%2C%20Am%20Fasanengarten%205%2C%2076131%20Karlsruhe!5e0!3m2!1sde!2sde!4v1665945861639!5m2!1sde!2sde" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

### Shooters
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.0917299707226!2d8.390612950875587!3d49.00883980434802!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479707faaf0ec66d%3A0x2023eb891fd01ba4!2sShooter%20Stars%20Karlsruhe!5e0!3m2!1sde!2sde!4v1633523675661!5m2!1sde!2sde" width="100%" height="200" frameborder="1" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
{{% /expand%}}

## Dienstag
{{%expand "Infos" %}}
### Kippe
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.15977588297!2d8.417130950800598!3d49.007545198046024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4797063081bf4de5%3A0x7f7e6087ff003867!2sKippe%2023!5e0!3m2!1sde!2sde!4v1632737504612!5m2!1sde!2sde" width="100%" height="200" frameborder="1" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

### Oxford Pub
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.103798428563!2d8.410728450800665!3d49.00861019797102!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479706379472105d%3A0x823aadf9ebe8bb1d!2sOxford%20Pub%20-%20Bier%20%26%20Burger!5e0!3m2!1sde!2sde!4v1632737557033!5m2!1sde!2sde" width="100%" height="200" frameborder="1" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

### AKK
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2616.9725668321366!2d8.41281131608923!3d49.01110687930279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4797063259717ec1%3A0x343954b4efbf72b!2sAKK%20-%20Arbeitskreis%20Kultur%20und%20Kommunikation!5e0!3m2!1sde!2sde!4v1665945696679!5m2!1sde!2sde" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
{{% /expand%}}

## Mittwoch
{{%expand "Infos" %}}

### Marianne's Flammkuchen
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2854.091686589887!2d8.363444452263433!3d49.00652084051227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479706ec376bd1b1%3A0x8731c254035c568d!2sMarianne&#39;s%20Flammkuchen%20Karlsruhe!5e0!3m2!1sde!2sde!4v1632734100451!5m2!1sde!2sde" width="100%" height="200" frameborder="1" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

### Culteum
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.0138341185634!2d8.422522516089202!3d49.010321779302714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4797062e3a0dd847%3A0x28add8398963f1bf!2sCULTeum!5e0!3m2!1sde!2sde!4v1665945819427!5m2!1sde!2sde" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
{{% /expand%}}

## Donnerstag
{{%expand "Infos" %}}

### Aposto
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.0952939812505!2d8.39413275080071!3d49.00877199795961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4797065aa1ad17c9%3A0x5d8bd46334fe4269!2sAposto%20Karlsruhe!5e0!3m2!1sde!2sde!4v1632737672872!5m2!1sde!2sde" width="100%" height="200" frameborder="1" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

### Agostea
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.279092562094!2d8.410429016089106!3d49.0052750793022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479706388a6bdcab%3A0x16d47105820f0a84!2sAgostea%20Karlsruhe!5e0!3m2!1sde!2sde!4v1665945756175!5m2!1sde!2sde" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
{{% /expand%}}

## Freitag
{{%expand "Infos" %}}

### Krokokeller
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.1274194477755!2d8.393649750800657!3d49.00816079800255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4797065a95f7f747%3A0x51197353e5e7203c!2sKrokokeller!5e0!3m2!1sde!2sde!4v1632737597514!5m2!1sde!2sde" width="100%" height="200" frameborder="1" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
{{% /expand%}}
