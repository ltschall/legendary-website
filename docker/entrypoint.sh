#!/usr/bin/env sh
env_file=/usr/share/nginx/html/scripts/env.js

if [ "$BACKEND_URL" ]
then
  rm $env_file
  echo "const backend_url = '${BACKEND_URL}';" > $env_file

  echo "backend_url set to ${BACKEND_URL}"
else
  echo "BACKEND_URL not set, leaving backend_url unchanged"
fi

exec "$@"
